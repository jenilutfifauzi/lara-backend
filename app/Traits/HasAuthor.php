<?php

namespace App\Traits;

// use Illuminate\Foundation\Auth\User;
use App\Models\User;
use App\Models\Article;

/**
 * 
 */
trait HasAuthor
{
    public function author(): User
    {
        return $this->authorRelation;
    }

    public function authorRelation(): User
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function isAuthoredBy(User $user): bool
    {
        return $this->author()->matches($user);
    }

    public function authoredBy(User $author)
    {
        $this->authorRelation()->associate($author);
    }
}
